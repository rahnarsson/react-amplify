import React, { useState, useEffect } from 'react';
import './App.css';
import { API, Storage } from 'aws-amplify';
import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react';
import { listAssets } from './graphql/queries';
import { createAsset as createAssetMutation, deleteAsset as deleteAssetMutation } from './graphql/mutations';

const initialFormState = { name: '', description: '' }

function App() {
  const [Assets, setAssets] = useState([]);
  const [formData, setFormData] = useState(initialFormState);

  useEffect(() => {
    fetchAssets();
  }, []);


  async function onChange(e) {
    if (!e.target.files[0]) return
    const file = e.target.files[0];
    setFormData({ ...formData, media: file.name });
    const folder = (formData.name)
    const path = 'assets' + '/' + folder + '/'
    console.log('Folder: ' + folder)
    await Storage.put(path + file.name, file);
    fetchAssets();
  }

  async function fetchAssets() {
    const apiData = await API.graphql({ query: listAssets });
    const assetsFromAPI = apiData.data.listAssets.items;
    await Promise.all(assetsFromAPI.map(async asset => {
      if (asset.media) {
        const media = await Storage.get(asset.media);
        asset.media = media;
      }
      console.log(asset)
      return asset;
    }))
    setAssets(apiData.data.listAssets.items);
  }

  async function createAsset() {
    if (!formData.name || !formData.description) return;
    await API.graphql({ query: createAssetMutation, variables: { input: formData } });
    if (formData.media) {
      const media = await Storage.get(formData.media)
      formData.media = media
    }
    setAssets([ ...Assets, formData ]);
    setFormData(initialFormState);
  }

  async function deleteAsset({ id }) {
    const newAssetsArray = Assets.filter(Asset => Asset.id !== id);
    setAssets(newAssetsArray);
    await API.graphql({ query: deleteAssetMutation, variables: { input: { id } }});
  }

  return (
    <div className="App" style={styles.container}>
      <h1>My Assets App</h1>
      <input
        onChange={e => setFormData({ ...formData, 'name': e.target.value})}
        placeholder="Asset name"
        value={formData.name}
        style={styles.input}
      />
      <input
        onChange={e => setFormData({ ...formData, 'description': e.target.value})}
        placeholder="Asset description"
        value={formData.description}
        style={styles.input}
      />
      <input
        type="file"
        onChange={onChange}
      />
      <button onClick={createAsset}>Create Asset</button>
      <div style={{marginBottom: 30}}>
        {
          Assets.map(Asset => (
            <div key={Asset.id || Asset.name}>
              <h2>{Asset.name}</h2>
              <p>{Asset.description}</p>
              <button onClick={() => deleteAsset(Asset)}>Delete Asset</button>
              {
                Asset.media && <img src={Asset.media} style={{width: 400}} />
              }

            </div>
          ))
        }
      </div>
      <AmplifySignOut />
    </div>
  )
}

const styles = {
  container: { width: 400, margin: '0 auto', display: 'flex', flexDirection: 'column', justifyContent: 'center', padding: 20 },
  todo: {  marginBottom: 15 },
  input: { border: 'none', backgroundColor: '#ddd', marginBottom: 10, padding: 8, fontSize: 18 },
  todoName: { fontSize: 20, fontWeight: 'bold' },
  todoDescription: { marginBottom: 0 },
  button: { backgroundColor: 'black', color: 'white', outline: 'none', fontSize: 18, padding: '12px 0px' }
}

export default withAuthenticator(App);